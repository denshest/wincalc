﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Calculator;
using System.Threading.Tasks;

namespace SimpleCalculator
{
    /// <summary>
    /// Summary description for CalcUI.
    /// </summary>
    public class CalcUI : System.Windows.Forms.Form
    {
        private System.Windows.Forms.TextBox VersionInfo;
        private System.Windows.Forms.Button KeyExit;
        private System.Windows.Forms.Button KeyDate;
        private System.Windows.Forms.TextBox OutputDisplay;
        private System.Windows.Forms.Button KeyClear;
        private System.Windows.Forms.Button KeyMinus;
        private System.Windows.Forms.Button KeyPlus;
        private System.Windows.Forms.Button KeyEqual;
        private System.Windows.Forms.Button KeyMultiply;
        private System.Windows.Forms.Button KeyDivide;
        private System.Windows.Forms.Button KeyPoint;
        private System.Windows.Forms.Button KeySign;
        private System.Windows.Forms.Button KeyZero;
        private System.Windows.Forms.Button KeyNine;
        private System.Windows.Forms.Button KeyEight;
        private System.Windows.Forms.Button KeySeven;
        private System.Windows.Forms.Button KeySix;
        private System.Windows.Forms.Button KeyFive;
        private System.Windows.Forms.Button KeyFour;
        private System.Windows.Forms.Button KeyThree;
        private System.Windows.Forms.Button KeyTwo;
        private System.Windows.Forms.Button KeyOne;

        // Output Display Constants.
        private const string oneOut = "1";
        private const string twoOut = "2";
        private const string threeOut = "3";
        private const string fourOut = "4";
        private const string fiveOut = "5";
        private const string sixOut = "6";
        private const string sevenOut = "7";
        private const string eightOut = "8";
        private const string nineOut = "9";
        private const string zeroOut = "0";
        private MenuStrip menuStrip1;
        private ToolStripMenuItem этоToolStripMenuItem;
        private ToolStripMenuItem обычныйToolStripMenuItem;
        private ToolStripMenuItem инженерныйToolStripMenuItem;
        private ToolStripMenuItem расширенныйToolStripMenuItem1;
        private ToolTip toolTip1;
        private Button degreeY;
        private Button Square;
        private Button InverseValue;
        private Button DegreeOf2;
        private FlowLayoutPanel flowLayoutPanel1;
        private Button Factor;
        private Button Square3;
        private Button SquareEq;
        private TextBox Equation;
        private Button Result;
        private FlowLayoutPanel flowLayoutPanel2;
        private TextBox FOut;
        private Label factorasync;
        private IContainer components;

        public CalcUI()
        {
            //
            // Required for Windows Form Designer support
            //

            InitializeComponent();

            //
            // Get version information from the Calculator Module.
            //

            VersionInfo.Text = CalcEngine.GetVersion();
            OutputDisplay.Text = "0";
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.KeyDate = new System.Windows.Forms.Button();
            this.KeyOne = new System.Windows.Forms.Button();
            this.VersionInfo = new System.Windows.Forms.TextBox();
            this.KeySix = new System.Windows.Forms.Button();
            this.KeyFive = new System.Windows.Forms.Button();
            this.KeyEqual = new System.Windows.Forms.Button();
            this.KeyTwo = new System.Windows.Forms.Button();
            this.KeyZero = new System.Windows.Forms.Button();
            this.KeyThree = new System.Windows.Forms.Button();
            this.KeyPlus = new System.Windows.Forms.Button();
            this.KeyExit = new System.Windows.Forms.Button();
            this.KeySign = new System.Windows.Forms.Button();
            this.KeySeven = new System.Windows.Forms.Button();
            this.KeyPoint = new System.Windows.Forms.Button();
            this.KeyNine = new System.Windows.Forms.Button();
            this.OutputDisplay = new System.Windows.Forms.TextBox();
            this.KeyMinus = new System.Windows.Forms.Button();
            this.KeyEight = new System.Windows.Forms.Button();
            this.KeyMultiply = new System.Windows.Forms.Button();
            this.KeyFour = new System.Windows.Forms.Button();
            this.KeyClear = new System.Windows.Forms.Button();
            this.KeyDivide = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.этоToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.обычныйToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.инженерныйToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.расширенныйToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.degreeY = new System.Windows.Forms.Button();
            this.Square = new System.Windows.Forms.Button();
            this.InverseValue = new System.Windows.Forms.Button();
            this.DegreeOf2 = new System.Windows.Forms.Button();
            this.Factor = new System.Windows.Forms.Button();
            this.Square3 = new System.Windows.Forms.Button();
            this.SquareEq = new System.Windows.Forms.Button();
            this.Result = new System.Windows.Forms.Button();
            this.factorasync = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Equation = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.FOut = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // KeyDate
            // 
            this.KeyDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyDate.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.KeyDate.Location = new System.Drawing.Point(65, 49);
            this.KeyDate.Name = "KeyDate";
            this.KeyDate.Size = new System.Drawing.Size(56, 40);
            this.KeyDate.TabIndex = 19;
            this.KeyDate.TabStop = false;
            this.KeyDate.Text = "Date";
            this.toolTip1.SetToolTip(this.KeyDate, "Текущая дата");
            this.KeyDate.UseVisualStyleBackColor = false;
            this.KeyDate.Click += new System.EventHandler(this.KeyDate_Click);
            // 
            // KeyOne
            // 
            this.KeyOne.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyOne.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyOne.ForeColor = System.Drawing.Color.Indigo;
            this.KeyOne.Location = new System.Drawing.Point(12, 263);
            this.KeyOne.Name = "KeyOne";
            this.KeyOne.Size = new System.Drawing.Size(40, 40);
            this.KeyOne.TabIndex = 2;
            this.KeyOne.TabStop = false;
            this.KeyOne.Text = "1";
            this.KeyOne.UseVisualStyleBackColor = false;
            this.KeyOne.Click += new System.EventHandler(this.KeyOne_Click);
            // 
            // VersionInfo
            // 
            this.VersionInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.VersionInfo.Cursor = System.Windows.Forms.Cursors.No;
            this.VersionInfo.Font = new System.Drawing.Font("Verdana", 8F);
            this.VersionInfo.Location = new System.Drawing.Point(12, 27);
            this.VersionInfo.Name = "VersionInfo";
            this.VersionInfo.ReadOnly = true;
            this.VersionInfo.Size = new System.Drawing.Size(306, 20);
            this.VersionInfo.TabIndex = 0;
            this.VersionInfo.TabStop = false;
            this.VersionInfo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // KeySix
            // 
            this.KeySix.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeySix.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeySix.ForeColor = System.Drawing.Color.Indigo;
            this.KeySix.Location = new System.Drawing.Point(108, 215);
            this.KeySix.Name = "KeySix";
            this.KeySix.Size = new System.Drawing.Size(40, 40);
            this.KeySix.TabIndex = 7;
            this.KeySix.TabStop = false;
            this.KeySix.Text = "6";
            this.KeySix.UseVisualStyleBackColor = false;
            this.KeySix.Click += new System.EventHandler(this.KeySix_Click);
            // 
            // KeyFive
            // 
            this.KeyFive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyFive.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyFive.ForeColor = System.Drawing.Color.Indigo;
            this.KeyFive.Location = new System.Drawing.Point(60, 215);
            this.KeyFive.Name = "KeyFive";
            this.KeyFive.Size = new System.Drawing.Size(40, 40);
            this.KeyFive.TabIndex = 6;
            this.KeyFive.TabStop = false;
            this.KeyFive.Text = "5";
            this.KeyFive.UseVisualStyleBackColor = false;
            this.KeyFive.Click += new System.EventHandler(this.KeyFive_Click);
            // 
            // KeyEqual
            // 
            this.KeyEqual.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyEqual.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyEqual.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.KeyEqual.Location = new System.Drawing.Point(67, 143);
            this.KeyEqual.Margin = new System.Windows.Forms.Padding(5);
            this.KeyEqual.Name = "KeyEqual";
            this.KeyEqual.Size = new System.Drawing.Size(56, 40);
            this.KeyEqual.TabIndex = 18;
            this.KeyEqual.TabStop = false;
            this.KeyEqual.Text = "=";
            this.toolTip1.SetToolTip(this.KeyEqual, "Результат (с задержкой)");
            this.KeyEqual.UseVisualStyleBackColor = false;
            this.KeyEqual.Click += new System.EventHandler(this.KeyEqual_Click);
            // 
            // KeyTwo
            // 
            this.KeyTwo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyTwo.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyTwo.ForeColor = System.Drawing.Color.Indigo;
            this.KeyTwo.Location = new System.Drawing.Point(60, 263);
            this.KeyTwo.Name = "KeyTwo";
            this.KeyTwo.Size = new System.Drawing.Size(40, 40);
            this.KeyTwo.TabIndex = 3;
            this.KeyTwo.TabStop = false;
            this.KeyTwo.Text = "2";
            this.KeyTwo.UseVisualStyleBackColor = false;
            this.KeyTwo.Click += new System.EventHandler(this.KeyTwo_Click);
            // 
            // KeyZero
            // 
            this.KeyZero.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyZero.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyZero.ForeColor = System.Drawing.Color.Indigo;
            this.KeyZero.Location = new System.Drawing.Point(12, 311);
            this.KeyZero.Name = "KeyZero";
            this.KeyZero.Size = new System.Drawing.Size(40, 40);
            this.KeyZero.TabIndex = 11;
            this.KeyZero.TabStop = false;
            this.KeyZero.Text = "0";
            this.KeyZero.UseVisualStyleBackColor = false;
            this.KeyZero.Click += new System.EventHandler(this.KeyZero_Click);
            // 
            // KeyThree
            // 
            this.KeyThree.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyThree.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyThree.ForeColor = System.Drawing.Color.Indigo;
            this.KeyThree.Location = new System.Drawing.Point(108, 263);
            this.KeyThree.Name = "KeyThree";
            this.KeyThree.Size = new System.Drawing.Size(40, 40);
            this.KeyThree.TabIndex = 4;
            this.KeyThree.TabStop = false;
            this.KeyThree.Text = "3";
            this.KeyThree.UseVisualStyleBackColor = false;
            this.KeyThree.Click += new System.EventHandler(this.KeyThree_Click);
            // 
            // KeyPlus
            // 
            this.KeyPlus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyPlus.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyPlus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.KeyPlus.Location = new System.Drawing.Point(156, 311);
            this.KeyPlus.Name = "KeyPlus";
            this.KeyPlus.Size = new System.Drawing.Size(40, 40);
            this.KeyPlus.TabIndex = 12;
            this.KeyPlus.TabStop = false;
            this.KeyPlus.Text = "+";
            this.toolTip1.SetToolTip(this.KeyPlus, "Сложение");
            this.KeyPlus.UseVisualStyleBackColor = false;
            this.KeyPlus.Click += new System.EventHandler(this.KeyPlus_Click);
            // 
            // KeyExit
            // 
            this.KeyExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyExit.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyExit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.KeyExit.Location = new System.Drawing.Point(65, 95);
            this.KeyExit.Name = "KeyExit";
            this.KeyExit.Size = new System.Drawing.Size(56, 40);
            this.KeyExit.TabIndex = 21;
            this.KeyExit.TabStop = false;
            this.KeyExit.Text = "Exit";
            this.toolTip1.SetToolTip(this.KeyExit, "Выход");
            this.KeyExit.UseVisualStyleBackColor = false;
            this.KeyExit.Click += new System.EventHandler(this.KeyExit_Click);
            // 
            // KeySign
            // 
            this.KeySign.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeySign.Font = new System.Drawing.Font("Courier New", 8F, System.Drawing.FontStyle.Bold);
            this.KeySign.ForeColor = System.Drawing.Color.Indigo;
            this.KeySign.Location = new System.Drawing.Point(108, 311);
            this.KeySign.Name = "KeySign";
            this.KeySign.Size = new System.Drawing.Size(40, 40);
            this.KeySign.TabIndex = 16;
            this.KeySign.TabStop = false;
            this.KeySign.Text = "+/-";
            this.KeySign.UseVisualStyleBackColor = false;
            this.KeySign.Click += new System.EventHandler(this.KeySign_Click);
            // 
            // KeySeven
            // 
            this.KeySeven.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeySeven.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeySeven.ForeColor = System.Drawing.Color.Indigo;
            this.KeySeven.Location = new System.Drawing.Point(12, 167);
            this.KeySeven.Name = "KeySeven";
            this.KeySeven.Size = new System.Drawing.Size(40, 40);
            this.KeySeven.TabIndex = 8;
            this.KeySeven.TabStop = false;
            this.KeySeven.Text = "7";
            this.KeySeven.UseVisualStyleBackColor = false;
            this.KeySeven.Click += new System.EventHandler(this.KeySeven_Click);
            // 
            // KeyPoint
            // 
            this.KeyPoint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyPoint.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyPoint.ForeColor = System.Drawing.Color.Indigo;
            this.KeyPoint.Location = new System.Drawing.Point(60, 311);
            this.KeyPoint.Name = "KeyPoint";
            this.KeyPoint.Size = new System.Drawing.Size(40, 40);
            this.KeyPoint.TabIndex = 17;
            this.KeyPoint.TabStop = false;
            this.KeyPoint.Text = ".";
            this.KeyPoint.UseVisualStyleBackColor = false;
            this.KeyPoint.Click += new System.EventHandler(this.KeyPoint_Click);
            // 
            // KeyNine
            // 
            this.KeyNine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyNine.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyNine.ForeColor = System.Drawing.Color.Indigo;
            this.KeyNine.Location = new System.Drawing.Point(108, 167);
            this.KeyNine.Name = "KeyNine";
            this.KeyNine.Size = new System.Drawing.Size(40, 40);
            this.KeyNine.TabIndex = 10;
            this.KeyNine.TabStop = false;
            this.KeyNine.Text = "9";
            this.KeyNine.UseVisualStyleBackColor = false;
            this.KeyNine.Click += new System.EventHandler(this.KeyNine_Click);
            // 
            // OutputDisplay
            // 
            this.OutputDisplay.BackColor = System.Drawing.Color.White;
            this.OutputDisplay.Cursor = System.Windows.Forms.Cursors.No;
            this.OutputDisplay.Enabled = false;
            this.OutputDisplay.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold);
            this.OutputDisplay.ForeColor = System.Drawing.SystemColors.Info;
            this.OutputDisplay.Location = new System.Drawing.Point(3, 116);
            this.OutputDisplay.Name = "OutputDisplay";
            this.OutputDisplay.ReadOnly = true;
            this.OutputDisplay.Size = new System.Drawing.Size(306, 26);
            this.OutputDisplay.TabIndex = 1;
            this.OutputDisplay.TabStop = false;
            this.OutputDisplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // KeyMinus
            // 
            this.KeyMinus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyMinus.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyMinus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.KeyMinus.Location = new System.Drawing.Point(156, 263);
            this.KeyMinus.Name = "KeyMinus";
            this.KeyMinus.Size = new System.Drawing.Size(40, 40);
            this.KeyMinus.TabIndex = 13;
            this.KeyMinus.TabStop = false;
            this.KeyMinus.Text = "-";
            this.toolTip1.SetToolTip(this.KeyMinus, "Вычитание");
            this.KeyMinus.UseVisualStyleBackColor = false;
            this.KeyMinus.Click += new System.EventHandler(this.KeyMinus_Click);
            // 
            // KeyEight
            // 
            this.KeyEight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyEight.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyEight.ForeColor = System.Drawing.Color.Indigo;
            this.KeyEight.Location = new System.Drawing.Point(60, 167);
            this.KeyEight.Name = "KeyEight";
            this.KeyEight.Size = new System.Drawing.Size(40, 40);
            this.KeyEight.TabIndex = 9;
            this.KeyEight.TabStop = false;
            this.KeyEight.Text = "8";
            this.KeyEight.UseVisualStyleBackColor = false;
            this.KeyEight.Click += new System.EventHandler(this.KeyEight_Click);
            // 
            // KeyMultiply
            // 
            this.KeyMultiply.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyMultiply.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyMultiply.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.KeyMultiply.Location = new System.Drawing.Point(156, 215);
            this.KeyMultiply.Name = "KeyMultiply";
            this.KeyMultiply.Size = new System.Drawing.Size(40, 40);
            this.KeyMultiply.TabIndex = 14;
            this.KeyMultiply.TabStop = false;
            this.KeyMultiply.Text = "*";
            this.toolTip1.SetToolTip(this.KeyMultiply, "Умножение");
            this.KeyMultiply.UseVisualStyleBackColor = false;
            this.KeyMultiply.Click += new System.EventHandler(this.KeyMultiply_Click);
            // 
            // KeyFour
            // 
            this.KeyFour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyFour.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyFour.ForeColor = System.Drawing.Color.Indigo;
            this.KeyFour.Location = new System.Drawing.Point(12, 215);
            this.KeyFour.Name = "KeyFour";
            this.KeyFour.Size = new System.Drawing.Size(40, 40);
            this.KeyFour.TabIndex = 5;
            this.KeyFour.TabStop = false;
            this.KeyFour.Text = "4";
            this.KeyFour.UseVisualStyleBackColor = false;
            this.KeyFour.Click += new System.EventHandler(this.KeyFour_Click);
            // 
            // KeyClear
            // 
            this.KeyClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyClear.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyClear.ForeColor = System.Drawing.Color.Black;
            this.KeyClear.Location = new System.Drawing.Point(65, 3);
            this.KeyClear.Name = "KeyClear";
            this.KeyClear.Size = new System.Drawing.Size(56, 40);
            this.KeyClear.TabIndex = 20;
            this.KeyClear.TabStop = false;
            this.KeyClear.Text = "C";
            this.toolTip1.SetToolTip(this.KeyClear, "Отчистить");
            this.KeyClear.UseVisualStyleBackColor = false;
            this.KeyClear.Click += new System.EventHandler(this.KeyClear_Click);
            // 
            // KeyDivide
            // 
            this.KeyDivide.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.KeyDivide.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.KeyDivide.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.KeyDivide.Location = new System.Drawing.Point(154, 167);
            this.KeyDivide.Name = "KeyDivide";
            this.KeyDivide.Size = new System.Drawing.Size(40, 40);
            this.KeyDivide.TabIndex = 15;
            this.KeyDivide.TabStop = false;
            this.KeyDivide.Text = "/";
            this.toolTip1.SetToolTip(this.KeyDivide, "Деление");
            this.KeyDivide.UseVisualStyleBackColor = false;
            this.KeyDivide.Click += new System.EventHandler(this.KeyDivide_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.этоToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(330, 24);
            this.menuStrip1.TabIndex = 26;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // этоToolStripMenuItem
            // 
            this.этоToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.обычныйToolStripMenuItem,
            this.инженерныйToolStripMenuItem,
            this.расширенныйToolStripMenuItem1});
            this.этоToolStripMenuItem.Name = "этоToolStripMenuItem";
            this.этоToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.этоToolStripMenuItem.Text = "Меню";
            // 
            // обычныйToolStripMenuItem
            // 
            this.обычныйToolStripMenuItem.Name = "обычныйToolStripMenuItem";
            this.обычныйToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.обычныйToolStripMenuItem.Text = "Обычный";
            this.обычныйToolStripMenuItem.Click += new System.EventHandler(this.обычныйToolStripMenuItem_Click);
            // 
            // инженерныйToolStripMenuItem
            // 
            this.инженерныйToolStripMenuItem.Name = "инженерныйToolStripMenuItem";
            this.инженерныйToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.инженерныйToolStripMenuItem.Text = "Инженерный";
            this.инженерныйToolStripMenuItem.ToolTipText = "Вычисление степеней и квадратов чисел";
            this.инженерныйToolStripMenuItem.Click += new System.EventHandler(this.инженерныйToolStripMenuItem_Click);
            // 
            // расширенныйToolStripMenuItem1
            // 
            this.расширенныйToolStripMenuItem1.Name = "расширенныйToolStripMenuItem1";
            this.расширенныйToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.расширенныйToolStripMenuItem1.Text = "Расширенный";
            this.расширенныйToolStripMenuItem1.ToolTipText = "Расширенные возможности";
            this.расширенныйToolStripMenuItem1.Click += new System.EventHandler(this.расширенныйToolStripMenuItem1_Click);
            // 
            // degreeY
            // 
            this.degreeY.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.degreeY.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.degreeY.ForeColor = System.Drawing.Color.Black;
            this.degreeY.Location = new System.Drawing.Point(3, 49);
            this.degreeY.Name = "degreeY";
            this.degreeY.Size = new System.Drawing.Size(56, 40);
            this.degreeY.TabIndex = 28;
            this.degreeY.TabStop = false;
            this.degreeY.Text = "x^y";
            this.toolTip1.SetToolTip(this.degreeY, "Возведение числа в степень");
            this.degreeY.UseVisualStyleBackColor = false;
            this.degreeY.Visible = false;
            this.degreeY.Click += new System.EventHandler(this.degreeY_Click_1);
            // 
            // Square
            // 
            this.Square.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Square.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.Square.ForeColor = System.Drawing.Color.Black;
            this.Square.Location = new System.Drawing.Point(3, 3);
            this.Square.Name = "Square";
            this.Square.Size = new System.Drawing.Size(56, 40);
            this.Square.TabIndex = 29;
            this.Square.TabStop = false;
            this.Square.Text = "√x";
            this.toolTip1.SetToolTip(this.Square, "Извлечение квадратного корня");
            this.Square.UseVisualStyleBackColor = false;
            this.Square.Visible = false;
            this.Square.Click += new System.EventHandler(this.SquareRoot_Click_1);
            // 
            // InverseValue
            // 
            this.InverseValue.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.InverseValue.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.InverseValue.ForeColor = System.Drawing.Color.Black;
            this.InverseValue.Location = new System.Drawing.Point(3, 95);
            this.InverseValue.Name = "InverseValue";
            this.InverseValue.Size = new System.Drawing.Size(56, 40);
            this.InverseValue.TabIndex = 30;
            this.InverseValue.TabStop = false;
            this.InverseValue.Text = "1/x";
            this.toolTip1.SetToolTip(this.InverseValue, "Обратное значение");
            this.InverseValue.UseVisualStyleBackColor = false;
            this.InverseValue.Visible = false;
            this.InverseValue.Click += new System.EventHandler(this.InverseValue_Click_1);
            // 
            // DegreeOf2
            // 
            this.DegreeOf2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.DegreeOf2.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.DegreeOf2.ForeColor = System.Drawing.Color.Black;
            this.DegreeOf2.Location = new System.Drawing.Point(3, 141);
            this.DegreeOf2.Name = "DegreeOf2";
            this.DegreeOf2.Size = new System.Drawing.Size(56, 40);
            this.DegreeOf2.TabIndex = 31;
            this.DegreeOf2.TabStop = false;
            this.DegreeOf2.Text = "x^2";
            this.toolTip1.SetToolTip(this.DegreeOf2, "Определение квадрата числа");
            this.DegreeOf2.UseVisualStyleBackColor = false;
            this.DegreeOf2.Visible = false;
            this.DegreeOf2.Click += new System.EventHandler(this.DegreeOf2_Click_1);
            // 
            // Factor
            // 
            this.Factor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Factor.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.Factor.ForeColor = System.Drawing.Color.Black;
            this.Factor.Location = new System.Drawing.Point(3, 70);
            this.Factor.Name = "Factor";
            this.Factor.Size = new System.Drawing.Size(53, 40);
            this.Factor.TabIndex = 32;
            this.Factor.TabStop = false;
            this.Factor.Text = "x!";
            this.toolTip1.SetToolTip(this.Factor, "Факториал числа");
            this.Factor.UseVisualStyleBackColor = false;
            this.Factor.Visible = false;
            this.Factor.Click += new System.EventHandler(this.factor_Click);
            // 
            // Square3
            // 
            this.Square3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Square3.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.Square3.ForeColor = System.Drawing.Color.Black;
            this.Square3.Location = new System.Drawing.Point(62, 70);
            this.Square3.Name = "Square3";
            this.Square3.Size = new System.Drawing.Size(56, 40);
            this.Square3.TabIndex = 33;
            this.Square3.TabStop = false;
            this.Square3.Text = "3√x";
            this.toolTip1.SetToolTip(this.Square3, "Расчет кубического корня");
            this.Square3.UseVisualStyleBackColor = false;
            this.Square3.Visible = false;
            this.Square3.Click += new System.EventHandler(this.Square3_Click);
            // 
            // SquareEq
            // 
            this.SquareEq.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.SquareEq.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.SquareEq.ForeColor = System.Drawing.Color.Black;
            this.SquareEq.Location = new System.Drawing.Point(124, 70);
            this.SquareEq.Name = "SquareEq";
            this.SquareEq.Size = new System.Drawing.Size(172, 40);
            this.SquareEq.TabIndex = 34;
            this.SquareEq.TabStop = false;
            this.SquareEq.Text = "Квадратное уравнение";
            this.toolTip1.SetToolTip(this.SquareEq, "Режим решения квадратного уравнения!");
            this.SquareEq.UseVisualStyleBackColor = false;
            this.SquareEq.Visible = false;
            this.SquareEq.Click += new System.EventHandler(this.button3_Click);
            // 
            // Result
            // 
            this.Result.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Result.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.Result.ForeColor = System.Drawing.Color.Black;
            this.Result.Location = new System.Drawing.Point(213, 3);
            this.Result.Name = "Result";
            this.Result.Size = new System.Drawing.Size(96, 32);
            this.Result.TabIndex = 40;
            this.Result.TabStop = false;
            this.Result.Text = "Добавить a";
            this.toolTip1.SetToolTip(this.Result, "Добавить коэффициент а");
            this.Result.UseVisualStyleBackColor = false;
            this.Result.Visible = false;
            this.Result.Click += new System.EventHandler(this.result_Click);
            // 
            // factorasync
            // 
            this.factorasync.AutoSize = true;
            this.factorasync.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.factorasync.Location = new System.Drawing.Point(3, 38);
            this.factorasync.Name = "factorasync";
            this.factorasync.Size = new System.Drawing.Size(145, 20);
            this.factorasync.TabIndex = 42;
            this.factorasync.Text = "Факториал числа";
            this.toolTip1.SetToolTip(this.factorasync, "Факториал числа рассчитывается при проведении операции возведение в квадрат");
            this.factorasync.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.Square);
            this.flowLayoutPanel1.Controls.Add(this.degreeY);
            this.flowLayoutPanel1.Controls.Add(this.InverseValue);
            this.flowLayoutPanel1.Controls.Add(this.DegreeOf2);
            this.flowLayoutPanel1.Controls.Add(this.KeyClear);
            this.flowLayoutPanel1.Controls.Add(this.KeyDate);
            this.flowLayoutPanel1.Controls.Add(this.KeyExit);
            this.flowLayoutPanel1.Controls.Add(this.KeyEqual);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(202, 167);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(128, 196);
            this.flowLayoutPanel1.TabIndex = 32;
            // 
            // Equation
            // 
            this.Equation.BackColor = System.Drawing.Color.White;
            this.Equation.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Bold);
            this.Equation.ForeColor = System.Drawing.SystemColors.Desktop;
            this.Equation.Location = new System.Drawing.Point(3, 3);
            this.Equation.Name = "Equation";
            this.Equation.ReadOnly = true;
            this.Equation.Size = new System.Drawing.Size(204, 26);
            this.Equation.TabIndex = 35;
            this.Equation.TabStop = false;
            this.Equation.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.Equation.Visible = false;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.Equation);
            this.flowLayoutPanel2.Controls.Add(this.Result);
            this.flowLayoutPanel2.Controls.Add(this.factorasync);
            this.flowLayoutPanel2.Controls.Add(this.FOut);
            this.flowLayoutPanel2.Controls.Add(this.Factor);
            this.flowLayoutPanel2.Controls.Add(this.Square3);
            this.flowLayoutPanel2.Controls.Add(this.SquareEq);
            this.flowLayoutPanel2.Controls.Add(this.OutputDisplay);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(12, 50);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(315, 114);
            this.flowLayoutPanel2.TabIndex = 41;
            // 
            // FOut
            // 
            this.FOut.BackColor = System.Drawing.Color.White;
            this.FOut.Font = new System.Drawing.Font("Courier New", 10F, System.Drawing.FontStyle.Bold);
            this.FOut.ForeColor = System.Drawing.SystemColors.Desktop;
            this.FOut.Location = new System.Drawing.Point(154, 41);
            this.FOut.Name = "FOut";
            this.FOut.ReadOnly = true;
            this.FOut.Size = new System.Drawing.Size(133, 23);
            this.FOut.TabIndex = 41;
            this.FOut.TabStop = false;
            this.FOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.FOut.Visible = false;
            // 
            // CalcUI
            // 
            this.AcceptButton = this.KeyZero;
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(330, 362);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.KeyOne);
            this.Controls.Add(this.KeyTwo);
            this.Controls.Add(this.KeyThree);
            this.Controls.Add(this.KeyFour);
            this.Controls.Add(this.KeyFive);
            this.Controls.Add(this.KeySix);
            this.Controls.Add(this.KeySeven);
            this.Controls.Add(this.KeyEight);
            this.Controls.Add(this.KeyNine);
            this.Controls.Add(this.KeyZero);
            this.Controls.Add(this.KeyPlus);
            this.Controls.Add(this.KeyMinus);
            this.Controls.Add(this.KeyMultiply);
            this.Controls.Add(this.KeyDivide);
            this.Controls.Add(this.KeySign);
            this.Controls.Add(this.KeyPoint);
            this.Controls.Add(this.VersionInfo);
            this.Controls.Add(this.menuStrip1);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "CalcUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculator";
            this.TopMost = true;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        protected void KeyPlus_Click(object sender, System.EventArgs e)
        {
            CalcEngine.CalcOperation(CalcEngine.Operator.eAdd);
        }

        protected void KeyMinus_Click(object sender, System.EventArgs e)
        {
            CalcEngine.CalcOperation(CalcEngine.Operator.eSubtract);
        }

        protected void KeyMultiply_Click(object sender, System.EventArgs e)
        {
            CalcEngine.CalcOperation(CalcEngine.Operator.eMultiply);
        }

        protected void KeyDivide_Click(object sender, System.EventArgs e)
        {
            CalcEngine.CalcOperation(CalcEngine.Operator.eDivide);
        }
        private void degreeY_Click_1(object sender, EventArgs e)
        {
            CalcEngine.CalcOperation(CalcEngine.Operator.eDegreeY);
        }
        private void SquareRoot_Click_1(object sender, EventArgs e)
        {
            CalcEngine.CalcOperationOne(CalcEngine.Operator.eSquare);
        }

        private async void InverseValue_Click_1(object sender, EventArgs e)
        {
            int res = await Factorial();
            CalcEngine.CalcOperationOne(CalcEngine.Operator.eInversX);
            FOut.Text = res.ToString();
        }

        private void DegreeOf2_Click_1(object sender, EventArgs e)
        {
            CalcEngine.CalcOperationOne(CalcEngine.Operator.eDegree2);
        }

        private async Task<int> Factorial()
        {
            return await Task.Run(() =>
            {
                return Convert.ToInt32(CalcEngine.Factorialasync());
            }
            );
        }

        private void factor_Click(object sender, EventArgs e)
        {
            CalcEngine.CalcOperationOne(CalcEngine.Operator.eFacrotial);
        }
        private void Square3_Click(object sender, EventArgs e)
        {
            CalcEngine.CalcOperationOne(CalcEngine.Operator.eSquare3);
        }
        //
        // Other non-numeric key click methods.
        //

        protected void KeySign_Click(object sender, System.EventArgs e)
        {
            if (this.Equation.Visible == true)
            {
                this.Equation.Text = CalcEngine.CalcSign();
            }
            else
            {
                OutputDisplay.Text = CalcEngine.CalcSign();
            }

        }

        protected void KeyPoint_Click(object sender, System.EventArgs e)
        {
            if (this.Equation.Visible == true)
            {
                this.Equation.Text = CalcEngine.CalcDecimal();
            }
            else
            {
                OutputDisplay.Text = CalcEngine.CalcDecimal();
            }

        }

        protected void KeyDate_Click(object sender, System.EventArgs e)
        {
            OutputDisplay.Text = CalcEngine.GetDate();
            CalcEngine.CalcReset();
        }

        protected void KeyClear_Click(object sender, System.EventArgs e)
        {
            CalcEngine.CalcReset();
            OutputDisplay.Text = "0";
        }

        //
        // Perform the calculation.
        //

        protected void KeyEqual_Click(object sender, System.EventArgs e)
        {
            OutputDisplay.Text = CalcEngine.CalcEqual();
            CalcEngine.CalcReset();
        }

        //
        // Numeric key click methods.
        //

        protected void KeyNine_Click(object sender, System.EventArgs e)
        {
            if (this.Equation.Visible == true)
            {
                this.Equation.Text = CalcEngine.CalcNumber(nineOut);
            }
            else
            {
                OutputDisplay.Text = CalcEngine.CalcNumber(nineOut);
            }
        }

        protected void KeyEight_Click(object sender, System.EventArgs e)
        {
            if (this.Equation.Visible == true)
            {
                this.Equation.Text = CalcEngine.CalcNumber(eightOut);
            }
            else
            {
                OutputDisplay.Text = CalcEngine.CalcNumber(eightOut);
            }
        }

        protected void KeySeven_Click(object sender, System.EventArgs e)
        {
            if (this.Equation.Visible == true)
            {
                this.Equation.Text = CalcEngine.CalcNumber(sevenOut);
            }
            else
            {
                OutputDisplay.Text = CalcEngine.CalcNumber(sevenOut);
            }
        }

        protected void KeySix_Click(object sender, System.EventArgs e)
        {
            if (this.Equation.Visible == true)
            {
                this.Equation.Text = CalcEngine.CalcNumber(sixOut);
            }
            else
            {
                OutputDisplay.Text = CalcEngine.CalcNumber(sixOut);
            }
        }

        protected void KeyFive_Click(object sender, System.EventArgs e)
        {
            if (this.Equation.Visible == true)
            {
                this.Equation.Text = CalcEngine.CalcNumber(fiveOut);
            }
            else
            {
                OutputDisplay.Text = CalcEngine.CalcNumber(fiveOut);
            }
        }

        protected void KeyFour_Click(object sender, System.EventArgs e)
        {
            if (this.Equation.Visible == true)
            {
                this.Equation.Text = CalcEngine.CalcNumber(fourOut);
            }
            else
            {
                OutputDisplay.Text = CalcEngine.CalcNumber(fourOut);
            }
        }

        protected void KeyThree_Click(object sender, System.EventArgs e)
        {
            if (this.Equation.Visible == true)
            {
                this.Equation.Text = CalcEngine.CalcNumber(threeOut);
            }
            else
            {
                OutputDisplay.Text = CalcEngine.CalcNumber(threeOut);
            }
        }

        protected void KeyTwo_Click(object sender, System.EventArgs e)
        {
            if (this.Equation.Visible == true)
            {
                this.Equation.Text = CalcEngine.CalcNumber(twoOut);
            }
            else
            {
                OutputDisplay.Text = CalcEngine.CalcNumber(twoOut);
            }
        }

        protected void KeyOne_Click(object sender, System.EventArgs e)
        {
            if (this.Equation.Visible == true)
            {
                this.Equation.Text = CalcEngine.CalcNumber(oneOut);
            }
            else
            {
                OutputDisplay.Text = CalcEngine.CalcNumber(oneOut);
            }

        }

        protected void KeyZero_Click(object sender, System.EventArgs e)
        {
            if (this.Equation.Visible == true)
            {
                this.Equation.Text = CalcEngine.CalcNumber(zeroOut);
            }
            else
            {
                OutputDisplay.Text = CalcEngine.CalcNumber(zeroOut);
            }
        }


        //
        // End the program.
        //

        protected void KeyExit_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new CalcUI());
        }


        private void обычныйToolStripMenuItem_Click(object sender, EventArgs e)
        {

            this.Equation.Visible = false;
            this.Result.Visible = false;
            this.Square3.Visible = false;
            this.Factor.Visible = false;
            this.SquareEq.Visible = false;
        }

        private void инженерныйToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.degreeY.Visible = true;
            this.DegreeOf2.Visible = true;
            this.InverseValue.Visible = true;
            this.Square.Visible = true;
            this.FOut.Visible = true;
            this.factorasync.Visible = true;
            this.Equation.Visible = false;
            this.Result.Visible = false;
            this.Square3.Visible = false;
            this.Factor.Visible = false;
            this.SquareEq.Visible = false;
        }
        private void расширенныйToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.degreeY.Visible = false;
            this.DegreeOf2.Visible = false;
            this.InverseValue.Visible = false;
            this.Square.Visible = false;
            this.Square3.Visible = true;
            this.Factor.Visible = true;
            this.SquareEq.Visible = true;
            this.FOut.Visible = false;
            this.factorasync.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Equation.Visible = !this.Equation.Visible;
            this.Result.Visible = !this.Result.Visible;
        }

        double? a = null;
        double? b = null;
        double? c = null;
        private void result_Click(object sender, EventArgs e)
        {
            if (a == null)
            {
                a = Convert.ToDouble(Equation.Text);
                this.Equation.Text = "0";
                this.Result.Text = "Добавить b";
                this.toolTip1.SetToolTip(this.Result, "Добавить коэффициент b");
                CalcEngine.AnswerReset();
                return;
            }
            else
            {
                if (b == null)
                {
                    b = Convert.ToDouble(Equation.Text);
                    this.Equation.Text = "0";
                    this.Result.Text = "Добавить c";
                    this.toolTip1.SetToolTip(this.Result, "Добавить коэффициент c и решить уравнение");
                    CalcEngine.AnswerReset();
                    return;
                }
                else
                {
                    if (c == null)
                    {
                        c = Convert.ToDouble(Equation.Text);
                        this.Result.Text = "New";
                        this.toolTip1.SetToolTip(this.Result, "Решить новое уравнение");
                    }
                    else
                    {
                        if (this.Result.Text == "New")
                        {
                            a = null;
                            b = null;
                            c = null;

                            this.Equation.Text = "0";
                            this.Result.Text = "Добавить a";
                            this.toolTip1.SetToolTip(this.Result, "Добавить коэффициент a");
                            CalcEngine.AnswerReset();
                            this.Equation.Text = CalcEngine.CalcEqual();
                            CalcEngine.CalcReset();
                            return;
                        }
                    }

                }
            }

            double d = Convert.ToDouble(b * b - 4 * a * c);
            if (d < 0)
            {
                this.Equation.Text = "Корней нет.";
            }
            else
            {
                if (d == 0)
                {
                    double x = Convert.ToDouble(-b / (2 * a));
                    this.Equation.Text = "Корень равен " + Math.Round(x, 3) + ".";
                }
                else
                {
                    double x1 = Convert.ToDouble((-b - Math.Sqrt(d)) / (2 * a));
                    double x2 = Convert.ToDouble((-b + Math.Sqrt(d)) / (2 * a));
                    this.Equation.Text = "x1= " + Math.Round(x1, 2) + "; x2= " + Math.Round(x2, 2) + ".";
                }
            }
            return;
        }

    }


}
